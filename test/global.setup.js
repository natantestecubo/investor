'use strict';

const assert = require('assert');
const sinon = require('sinon');
const nock = require('nock');
const proxyquire = require('proxyquire');

global.assert = assert;
global.sinon = sinon;
global.nock = nock;
global.proxyquire = proxyquire;
