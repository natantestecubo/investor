'use strict';

const should = require('chai').should();
const createHandler = require('../../src/create');
const proxyquire = require('proxyquire');

const createHandlerSuccess = proxyquire('../../src/create', {
  '../shared/lib/dynamo': {
    create: () => Promise.resolve({ firstName: 'Natan', lastName: 'Deitch', participation: 0.1, id: '1'})
  }
});

const createHandlerError = proxyquire('../../src/create', {
  '../shared/lib/dynamo': {
    create: () => Promise.reject({ statusCode: 400, message: 'ProvisionedThroughputExceededException' })
  }
});

describe('Create Handler Module', () => {
  it('Should return BAD_REQUEST if missing required properties', () => {
    return createHandler.create({ body: JSON.stringify({}) }).then(res => {
      should.equal(res.statusCode, 400);
    })
  });

  it('Should return investor with id for valid payload', () => {
    return createHandlerSuccess.create({
      body: JSON.stringify({
        firstName: 'Natan',
        lastName: 'Deitch',
        participation: 10
      })
    }).then(res => {
      should.equal(res.statusCode, 201);
      should.exist(JSON.parse(res.body).id);
    })
  });

  it('Should return bad request if provisioned throughput exceeded', () => {
    return createHandlerError.create({
      body: JSON.stringify({
        firstName: 'Natan',
        lastName: 'Deitch',
        participation: 10
      })
    }).then(res => {
      should.equal(res.statusCode, 400);
      should.exist(JSON.parse(res.body).error);
    })
  });
});
