'use strict';

const should = require('chai').should();
const createHandler = require('../../src/find');
const proxyquire = require('proxyquire');

const findHandlerSuccess = proxyquire('../../src/find', {
  '../shared/lib/dynamo': {
    findAll: () => Promise.resolve({
      Items: [{ name: 'Natan', lastName: 'Deitch', participation: 10, id: '1' }]
    })
  }
});

const noResultsHandler = proxyquire('../../src/find', {
  '../shared/lib/dynamo': {
    findAll: () => Promise.resolve({})
  }
});

const findHandlerError = proxyquire('../../src/find', {
  '../shared/lib/dynamo': {
    findAll: () => Promise.reject({ statusCode: 400, message: 'ProvisionedThroughputExceededException' })
  }
});

describe('Find Handler Module', () => {
  it('Should return investors', () => {
    return findHandlerSuccess.all().then(res => {
      should.equal(res.statusCode, 200);
      should.exist(JSON.parse(res.body).length);
    })
  });

  it('Should return empty array if no record on dynamo', () => {
    return noResultsHandler.all().then(res => {
      should.equal(res.statusCode, 200);
      should.equal(JSON.parse(res.body).length, 0);
    })
  });

  it('Should return bad request if provisioned throughput exceeded', () => {
    return findHandlerError.all().then(res => {
      should.equal(res.statusCode, 400);
      should.exist(JSON.parse(res.body).error);
    })
  });
});
