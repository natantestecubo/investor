'use strict';

const should = require('chai').should();
const proxyquire = require('proxyquire');

const DocumentStub = function() {}
DocumentStub.prototype.put = () => {
  return {
    promise: () => Promise.resolve()
  }
};

const mockDynamoSuccess = proxyquire('../../../shared/lib/dynamo', {
  'aws-sdk': {
    DynamoDB: {
      DocumentClient: DocumentStub
    }
  }
})

describe('DynamoDB Module', () => {
  it('Should create id, createdAt and updatedAt for new investor', () => {
    return mockDynamoSuccess.create({})
      .then((documentCreated) => {
        should.exist(documentCreated.id);
        should.exist(documentCreated.createdAt);
        should.exist(documentCreated.updatedAt);
      });
  });
});
