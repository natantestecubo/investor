'use strict';

const dynamo = require('../shared/lib/dynamo');

const create = (event) => {
  const investor = JSON.parse(event.body);

  if (!investor.firstName || !investor.lastName || !investor.participation) {
    return Promise.resolve({ statusCode: 400, });
  }

  investor.participation = investor.participation / 100;

  return new Promise((resolve) => {
    dynamo.create(investor)
      .then(investorCreated => {
        resolve({
          statusCode: 201,
          body: JSON.stringify(investorCreated),
          headers: {
            'Access-Control-Allow-Origin': '*',
          }
        });
      })
      .catch(error => {
        console.log('Error creating investor', error);
        resolve({
          statusCode: error.statusCode,
          body: JSON.stringify({error: error.message}),
          headers: {
            'Access-Control-Allow-Origin': '*',
          }
        });
      });
  });
};

module.exports = { create };
