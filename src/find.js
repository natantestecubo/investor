'use strict';

const dynamo = require('../shared/lib/dynamo');

const all = () => {
  return new Promise(resolve => {
    dynamo.findAll()
      .then(res => {
        const items = ((res || {}).Items || []);
        resolve({
          statusCode: 200,
          body: JSON.stringify(items),
          headers: {
            'Access-Control-Allow-Origin': '*',
          }
        });
      })
      .catch(error => {
        console.log('Error finding investors', error);
        resolve({
          statusCode: error.statusCode,
          body: JSON.stringify({error: error.message}),
          headers: {
            'Access-Control-Allow-Origin': '*',
          }
        });
      });
  });
};

module.exports = { all };
