[![serverless](http://public.serverless.com/badges/v3.svg)](http://www.serverless.com)

Bills API

It uses [serverless framework](https://github.com/serverless/serverless) and runs on AWS Lambda platform

## Quick Start

1. **Install via npm:**

  ```
    npm install -g serverless
  ```
  
  ```
    npm i
  ```
  
  ```
    sls dynamodb install --s prod --r sa-east-1
  ```

2. **Run locally:**

  ```
  npm run dev
  ```

3. **Deploy:**

  ```
  npm run deploy
  ```