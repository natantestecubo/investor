'use strict';

const crypto = require('crypto');
const { DynamoDB } = require('aws-sdk');
const moment = require('moment');
const table = process.env['TABLE'];
const dynamoOptions = {};

if (process.env.IS_OFFLINE) {
  dynamoOptions.endpoint = 'http://localhost:8000'
}

const dynamo = new DynamoDB.DocumentClient(dynamoOptions);

const create = (document) => {
  const now = moment().format('YYYY-MM-DD HH:mm:ss');
  document.id = generateId();
  document.createdAt = now;
  document.updatedAt = now;

  return dynamo.put({
    TableName: table,
    Item: document
  }).promise().then(() => document);
};

const findAll = () => {
  return dynamo.scan({
    TableName: table
  }).promise();
};

const generateId = () => {
  const current_date = (new Date()).valueOf().toString();
  const random = Math.random().toString();
  return crypto.createHash('sha1').update(current_date + random).digest('hex');
};

module.exports = { create, findAll };
